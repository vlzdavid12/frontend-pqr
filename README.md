![alt text](https://gitlab.com/vlzdavid12/frontend-pqr/-/raw/main/screenshot.png)

# Install Node Modules

```
  npm i or npm install
```

# Start Project
```
cd frondtendpqr
ng serve
```


# Url Project

```
  http://localhost:4200
```

# enviroment.ts

```
export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000'
};
```
