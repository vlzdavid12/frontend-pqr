import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {CraetePqrComponent} from './craete-pqr/craete-pqr.component';
import {ComponentsModule} from '../components/components.module';
import { ConsultPqrComponent } from './consult-pqr/consult-pqr.component';


@NgModule({
  declarations: [
    CraetePqrComponent,
    ConsultPqrComponent,
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    NgbModule
  ]
})
export class PagesModule {
}
