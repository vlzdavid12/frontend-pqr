import { Component, OnInit } from '@angular/core';
import {ServicePqrService} from '../../services/service-pqr.service';
import {PqrInterface} from '../../interfaces/pqr.interface';

@Component({
  selector: 'app-consult-pqr',
  templateUrl: './consult-pqr.component.html',
  styleUrls: ['./consult-pqr.component.scss']
})
export class ConsultPqrComponent implements OnInit {

  resultPQRS: PqrInterface[] = [];

  constructor(private servicePqr: ServicePqrService) { }

  ngOnInit(): void {
    this.getConsultPQRS();
  }

  getConsultPQRS(): void{
    this.servicePqr.getPqr().subscribe({
      next: (resp: PqrInterface[]) => {
        this.resultPQRS = resp;
      },
      error: (err => {
        console.log('getConsultPQR', err);
      })
    });
  }

}
