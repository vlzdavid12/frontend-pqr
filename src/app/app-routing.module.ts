import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ConsultPqrComponent} from './pages/consult-pqr/consult-pqr.component';
import {CraetePqrComponent} from './pages/craete-pqr/craete-pqr.component'; // CLI imports router

const routes: Routes = [{
  path: '',
  component: ConsultPqrComponent,
  pathMatch: 'full'
},
  {
    path: 'create',
    component: CraetePqrComponent
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
