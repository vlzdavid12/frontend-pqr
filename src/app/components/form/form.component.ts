import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {PostType, PqrInterface} from '../../interfaces/pqr.interface';
import {ServicePqrService} from '../../services/service-pqr.service';
import {filter} from 'rxjs/operators';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  @Input() editForm: boolean = false;
  @Input() dataFormId: string;

  formPQR: FormGroup;
  errorFromPQR: any[];
  successFullyForm: boolean = false;
  typePQR: string[] = [PostType.PETICION, PostType.QUEJA, PostType.RECLAMO];
  pathUrl!: string;

  constructor(private fb: FormBuilder,
              private servicePqr: ServicePqrService,
              private route: Router) {
    this.route.events.pipe(
      filter(event => event instanceof NavigationEnd))
      .subscribe((event: any) => {
        this.pathUrl = event.url.split('/')[1];
      });
  }

  inputValidate(input: string): boolean {
    return this.formPQR.controls[input].errors &&
      this.formPQR.controls[input].touched;
  }

  ngOnInit(): void {
    this.getPqrById();
    this.formPQR = this.fb.group({
      firstname: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(40)]],
      secondsname: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(40)]],
      phone: ['', [Validators.required, Validators.pattern('^((\\+91-?)|0)?[0-9]{10}$')]],
      email: ['', [Validators.required, Validators.minLength(6), Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      type: ['', [Validators.required]],
      comments: ['', [Validators.required, Validators.minLength(20), Validators.maxLength(240)]]
    });
  }

  getPqrById(): void {
    if (this.dataFormId) {
      this.servicePqr.getPqrById(this.dataFormId).subscribe({
        next: (resp) => {
          this.formPQR.controls.firstname.setValue(resp.firstName);
          this.formPQR.controls.secondsname.setValue(resp.lastName);
          this.formPQR.controls.phone.setValue(resp.phone);
          this.formPQR.controls.email.setValue(resp.email);
          this.formPQR.controls.type.setValue(resp.type);
          this.formPQR.controls.comments.setValue(resp.comments);
        },
        error: (error) => console.log('getPqrById', error)
      });
    }

  }

  saveForm(): void {
    const value = this.formPQR.value;
    value.firstName = value.firstname;
    value.lastName = value.secondsname;
    delete value.firstname;
    delete value.secondsname;
    if (this.formPQR.invalid) {
      this.formPQR.markAllAsTouched();
      return;
    } else if (this.editForm && this.dataFormId) {
      this.servicePqr.updatePqr(this.dataFormId, value).subscribe({
        complete: () => {
          this.successFullyForm = true;
          console.log('Edit data success fully');
        },
        error: (err) => console.log('Error Update: ', err),
      });
    } else {
      this.servicePqr.addPqr(value).subscribe({
        complete: () => {
          console.log('Insert Pqr Success Fully');
          this.successFullyForm = true;
          this.formPQR.reset();
        },
        error: ({error}) => {
          this.errorFromPQR = error.message;
        }
      });
    }


  }

}
