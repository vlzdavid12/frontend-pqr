import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormComponent} from './form/form.component';
import {TableComponent} from './table/table.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
    declarations: [
        FormComponent,
        TableComponent,
        LoaderComponent
    ],
  exports: [
    FormComponent,
    TableComponent
  ],
    imports: [
        CommonModule,
        NgbModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
    ]
})
export class ComponentsModule { }
