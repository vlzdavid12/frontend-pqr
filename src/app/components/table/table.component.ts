import {AfterContentChecked, Component, Input, ChangeDetectorRef} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {PqrInterface} from '../../interfaces/pqr.interface';
import {ServicePqrService} from '../../services/service-pqr.service';
import {delay, filter} from 'rxjs/operators';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements AfterContentChecked {
  @Input() pqrs: PqrInterface[];
  detailsPQR!: PqrInterface;
  loading: boolean = false;
  deleteNow: boolean = false;
  page = 1;
  pageSize = 4;
  collectionSize: number;
  resultPQRS: PqrInterface[];
  itemID!: string;

  constructor(private config: NgbModalConfig,
              private modalService: NgbModal,
              private servicePqr: ServicePqrService
  ) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngAfterContentChecked(): void {
    this.collectionSize = this.pqrs.length;
    this.refreshCountries();
  }

  refreshCountries(): void {
    if (this.itemID?.length > 0 && this.deleteNow) {
      this.pqrs = this.pqrs.filter(item => item._id !== this.itemID);
    }
    this.resultPQRS = this.pqrs
      .map((pqr, i) => ({id: i + 1, ...pqr}))
      .slice((this.page - 1) * this.pageSize, (this.page - 1) * this.pageSize + this.pageSize);
  }

  openScrollableContent(detailsContent, id: string): void {
    this.loading = true;
    this.deleteNow = false;
    this.servicePqr.getPqrById(id)
      .pipe(delay(500))
      .subscribe({
          next: (resp) => {
            this.detailsPQR = resp;
          },
          complete: () => {
            this.loading = false;
            console.log('Complete result details...');
          }
        }
      );
    this.modalService.open(detailsContent, {scrollable: false});
  }

  openConfirmDelete(confirmDelete, id: string): void {
    this.deleteNow = false;
    this.itemID = id;
    this.modalService.open(confirmDelete, {scrollable: false});
  }

  openConfirmEdit(confirmEdit, id: string): void {
    this.deleteNow = false;
    this.itemID = id;
    this.modalService.open(confirmEdit, {scrollable: true});
  }

  deleteNowItem(id: string): void {
    this.itemID = '';
    this.servicePqr.deletePqr(id).subscribe({
      next: () => this.itemID = id,
      complete: () => {
        this.deleteNow = true;
        this.modalService.dismissAll();
      },
    });
  }

  closeModalEdit(modal: any, id: string): void {
    let result;
    this.servicePqr.getPqrById(id).subscribe({
      next: (resp: PqrInterface) => {
        const filterResp: PqrInterface[] = [];
        this.pqrs.map(item => {
          if (item._id === resp._id){
              filterResp.push(resp);
              return;
          }
          filterResp.push(item);
        });
        result = filterResp;
      },
      complete: () => {
        this.pqrs = result;
        modal.dismiss('Cross click');
      },
    });



  }
}
