import { Component, OnInit } from '@angular/core';
import {Router, NavigationEnd} from '@angular/router';
import {filter} from 'rxjs/operators';
@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {

  pathUrl!: string;

  constructor(private route: Router) {
    this.route.events.pipe(
      filter(event => event instanceof NavigationEnd))
      .subscribe((event: any) =>
      {
        this.pathUrl = event.url.split('/')[1];
      });
  }


}
