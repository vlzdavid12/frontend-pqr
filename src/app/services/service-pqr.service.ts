import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {PqrInterface} from '../interfaces/pqr.interface';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServicePqrService {

  constructor(private http: HttpClient) { }

  getPqr(): Observable<PqrInterface[]>{
    return this.http.get<PqrInterface[]>(`${environment.apiUrl}/pqr`);
  }

  getPqrById(id: string): Observable<PqrInterface>{
    return this.http.get<PqrInterface>(`${environment.apiUrl}/pqr/${id}`);
  }

  addPqr(pqr: PqrInterface): Observable<PqrInterface>{
    return this.http.post<PqrInterface>(`${environment.apiUrl}/pqr`, pqr);
  }

  updatePqr(id: string, pqr: PqrInterface): Observable<PqrInterface>{
    return this.http.patch<PqrInterface>(`${environment.apiUrl}/pqr/${id}`, pqr );
  }

  deletePqr(id: string): Observable<any>{
    return this.http.delete(`${environment.apiUrl}/pqr/${id}`);
  }

}
