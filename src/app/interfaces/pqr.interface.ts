export enum PostType {
  RECLAMO = 'RECLAMO',
  QUEJA = 'QUEJA',
  PETICION = 'PETICION'
}

export interface PqrInterface {
  _id?: string;
  firstName: string;
  lastName: string;
  phone: number;
  email: string;
  type: PostType;
  comments: string;
}
